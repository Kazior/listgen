def comb(filename, combination = [], portion = 10, percent_step = 1)
	maxval = 25
	str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	combination = Array.new(8,0) if combination == []
	
	File.write(filename, '')

	puts "\n\n Preparing data... \n\n starting position: #{combination.inspect} \n\n"

	index = 0
	pos = 0
	counter = 0
	finecounter = 0

	counter_step = (portion / 100) * percent_step

	open(filename, 'a') do |f|
		loop do
			index = combination[pos] + 1
	
			if index > maxval
				combination[pos] = 0
				pos += 1
				next
			end

			combination[pos] = index
			pos = 0

			combination.each {|value| f.print str[value]}
			f.puts ''

			counter += 1
			finecounter += 1
		
			if finecounter > counter_step
				finecounter = 0
				mbytes = counter * 9 / 1024 / 1024
				percentage = (100 * counter) / portion
				puts "\e[2J\e[H" +
				"\n computing... \n\n" +
				' comb.    | step' +
				" #{translate(combination, str)} | #{counter} \n\n" +
				" #{mbytes.to_i} MBytes saved to file '#{filename}'" +
				" #{percentage}% completed \n\n"
			end

			break if counter == portion
		end
	end
	
	puts "\n end combination: #{combination}"
end

def translate(combination, string)
	output = ''

	combination.each do |value|
		output += string[value]
	end

	output
end
